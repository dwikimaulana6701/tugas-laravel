<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis(){
        return view('halaman.daftar');
    }

    public function kirim(Request $request){
        $fnama = $request['fname'];
        $lnama = $request['lname'];

        return view('halaman.selesei', compact('fnama','lnama'));
    }
}
